package com.kavapps.githubrepofinder.model;

import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;

import java.util.List;

public interface PublicRepoInteractor {

    void getPublicRepo(String firstFindString, String secondFindString, PublicRepoListener listener, int id);


    interface PublicRepoListener {
        void onResult(PublicRepositoriesResponse list);

        void onError(Throwable throwable);
    }

}
