package com.kavapps.githubrepofinder.model.impl;

import android.annotation.SuppressLint;

import com.kavapps.githubrepofinder.api.GitHubApi;
import com.kavapps.githubrepofinder.model.PublicRepoInteractor;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class PublicRepoInteractorImpl implements PublicRepoInteractor {

    private GitHubApi api;

    @Inject
    public PublicRepoInteractorImpl(GitHubApi api) {
        this.api = api;
    }


    @SuppressLint("CheckResult")
    @Override
    public void getPublicRepo(String firstFindString, String secondFindString, PublicRepoListener listener, int id) {
        // Берем первый поисковый запрос и пеебераем элементы.
        // Если имя полученного репозитория содержит (contains)
        // в себе наш поисковый запрос, то идем дальше и берем
        // только первые 15 элементов
        Observable<PublicRepositoriesResponse> firstFinderObservable = api.getPublicRepositories(id)
                .flatMap(new Function<List<PublicRepositoriesResponse>, Observable<PublicRepositoriesResponse>>() {
                    @Override
                    public Observable<PublicRepositoriesResponse> apply(List<PublicRepositoriesResponse> publicRepositoriesResponses) throws Exception {
                        return Observable.fromIterable(publicRepositoriesResponses);
                    }
                })
                .filter(new Predicate<PublicRepositoriesResponse>() {
                    @Override
                    public boolean test(PublicRepositoriesResponse publicRepositoriesResponse) throws Exception {
                        if (firstFindString.equals("")){
                            return false;
                        }else return publicRepositoriesResponse.getName().contains(firstFindString);
                    }
                })
                .take(15);
        // Берем второй поисковый запрос и пеебераем элементы.
        // Если имя полученного репозитория содержит (contains)
        // в себе наш поисковый запрос, то идем дальше и берем
        // только первые 15 элементов
        Observable<PublicRepositoriesResponse> secondFinderObservable = api.getPublicRepositories(id)
                .flatMap(new Function<List<PublicRepositoriesResponse>, Observable<PublicRepositoriesResponse>>() {
                    @Override
                    public Observable<PublicRepositoriesResponse> apply(List<PublicRepositoriesResponse> publicRepositoriesResponses) throws Exception {
                        return Observable.fromIterable(publicRepositoriesResponses);
                    }
                })
                .filter(new Predicate<PublicRepositoriesResponse>() {
                    @Override
                    public boolean test(PublicRepositoriesResponse publicRepositoriesResponse) throws Exception {
                        if (secondFindString.equals("")){
                            return false;
                        }else return publicRepositoriesResponse.getName().contains(secondFindString);
                    }
                })
                .take(15);
        // Объединяем оба Observable в один
        Observable.concat(firstFinderObservable,secondFinderObservable)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<PublicRepositoriesResponse>() {
                    @Override
                    public void accept(PublicRepositoriesResponse response) throws Exception {
                        if (listener != null) {
                            listener.onResult(response);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (listener != null) {
                            listener.onError(throwable);
                        }
                    }
                });
    }

}
