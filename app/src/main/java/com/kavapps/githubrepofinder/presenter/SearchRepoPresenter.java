package com.kavapps.githubrepofinder.presenter;

import android.content.Context;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kavapps.githubrepofinder.R;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;
import com.kavapps.githubrepofinder.model.PublicRepoInteractor;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;
import com.kavapps.githubrepofinder.view.SearchRepoView;


import javax.inject.Inject;


@InjectViewState
public class SearchRepoPresenter extends MvpPresenter<SearchRepoView> {

    private PublicRepoInteractor interactor;
    private DatabaseReference myRef;
    private static final String DATA_BASE_NAME = "favoriteRepo";
    private static final String TABLE_BASIC_NAME = "repo_id_";


    @Inject
    public SearchRepoPresenter(PublicRepoInteractor interactor) {
        this.interactor = interactor;

    }


    public void getListRepo(String firstFindString, String secondFindString, int id) {
        interactor.getPublicRepo(firstFindString, secondFindString, new PublicRepoInteractor.PublicRepoListener() {
            @Override
            public void onResult(PublicRepositoriesResponse list) {
                 getViewState().showListPublicRepo(list);
            }

            @Override
            public void onError(Throwable throwable) {
                getViewState().showMessage(throwable.toString());
            }
        },id);
    }


    public void addFavoriteRepoToDataBase(int id, String name, String description, String url, Context context){
        // Добавляем модель репозитория в "избранное"
        // Используем базу данных Firebase
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef.child(DATA_BASE_NAME).child(TABLE_BASIC_NAME+id).setValue(new FavoriteRepoModel(id,name,description,url));
        getViewState().showMessage(context.getString(R.string.repo_add_to_favorite_list_1)+" "+name+" "+context.getString(R.string.repo_add_to_favorite_list_2));
    }


}
