package com.kavapps.githubrepofinder.presenter;


import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;
import com.kavapps.githubrepofinder.view.FavoriteRepoView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;

@InjectViewState
public class FavoriteListRepoPresenter extends MvpPresenter<FavoriteRepoView> {

    private List<FavoriteRepoModel> favoriteRepoModelList = new ArrayList<>();
    private static final String DATA_BASE_NAME = "favoriteRepo";
    private static final String TABLE_BASIC_NAME = "repo_id_";

    @Inject
    public FavoriteListRepoPresenter() {

    }


    public void getListFavoriteRepo() {
        // Получаем список избанных репозиториев
        // из базы двнных Firebase и выводим в RecyclerView
        DatabaseReference  myRef = FirebaseDatabase.getInstance().getReference().child(DATA_BASE_NAME);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
               if (snapshot.exists()){
                   favoriteRepoModelList.clear();
                   for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                       FavoriteRepoModel favoriteRepoModel = dataSnapshot.getValue(FavoriteRepoModel.class);
                       favoriteRepoModelList.add(favoriteRepoModel);
                   }
                   getViewState().showListFavoriteRepo(favoriteRepoModelList);
               }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                getViewState().showMessage(error.getMessage());
            }
        });
    }

    public void deleteRepoFromList(int id){
        // Удаляем репозиторий из списа избанных
        // репозиториев и из базы двнных Firebase
        DatabaseReference  myRef = FirebaseDatabase.getInstance().getReference(DATA_BASE_NAME).child(TABLE_BASIC_NAME+id);
        myRef.removeValue();

        for (int i=0; i<favoriteRepoModelList.size();i++){
            if (favoriteRepoModelList.get(i).getId()==id){
                favoriteRepoModelList.remove(i);
                break;
            }
        }
        getViewState().showListFavoriteRepo(favoriteRepoModelList);
    }

}
