package com.kavapps.githubrepofinder.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kavapps.githubrepofinder.App;
import com.kavapps.githubrepofinder.R;
import com.kavapps.githubrepofinder.activity.ui.RepoAdapter;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;
import com.kavapps.githubrepofinder.presenter.SearchRepoPresenter;
import com.kavapps.githubrepofinder.view.SearchRepoView;

import javax.inject.Inject;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchRepoActivity extends MvpActivity implements SearchRepoView, RepoAdapter.AdapterListener {


    @BindView(R.id.buttonSearch)
    ImageButton buttonSearch;

    @BindView(R.id.recyclerViewListFindRepo)
    RecyclerView recyclerViewListFindRepo;

    @BindView(R.id.firstNeedToFindRepoEdit)
    EditText firstNeedToFindRepoEdit;

    @BindView(R.id.secondNeedToFindRepoEdit)
    EditText secondNeedToFindRepoEdit;

    private RepoAdapter repoAdapter;
    private static final int COME_FROM_FAVORITE_ACTIVITY_CODE = 9349;


    @Inject
    @InjectPresenter
    SearchRepoPresenter presenter;

    @ProvidePresenter
    SearchRepoPresenter providePresenter() {
        return presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_repo_activity);
        ButterKnife.bind(this);
        repoAdapter = new RepoAdapter(this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewListFindRepo.setItemAnimator(new DefaultItemAnimator());
        recyclerViewListFindRepo.setLayoutManager(mLayoutManager);
        recyclerViewListFindRepo.setAdapter(repoAdapter);
    }

    @Override
    public void showListPublicRepo(PublicRepositoriesResponse repo) {
        repoAdapter.setListRepo(repo);
    }

    @Override
    public void showMessage(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.buttonSearch)
    void onClickButtonSearch() {
        //Проверка, что хотя бы одно поле поиска заполнено
        if (firstNeedToFindRepoEdit.getText().toString().equals("") && secondNeedToFindRepoEdit.getText().toString().equals("")) {
            showMessage(getResources().getString(R.string.error_message_edit_null));
        } else {
            repoAdapter.clearList();
            repoAdapter.getFavoriteRepo();
            presenter.getListRepo(firstNeedToFindRepoEdit.getText().toString(), secondNeedToFindRepoEdit.getText().toString(), 1);
        }
    }

    @OnClick(R.id.back100Repo)
    void onClickButtonBack() {
        // github api выдает по 100 объектов, поэтому для получения
        // предыдущих 100 объектов берем первый элемент из списка и
        // если он больше 100, то вычитаем 100 (возвращаемся на 100
        // элементов назад), если меньше 100, то запрашиваем список
        // с первого элемента
        if (firstNeedToFindRepoEdit.getText().toString().equals("") && secondNeedToFindRepoEdit.getText().toString().equals("")) {
            showMessage(getResources().getString(R.string.error_message_edit_null));
        } else {
            int firsObject = repoAdapter.getFistObject();
            int id;
            if (firsObject > 100) {
                id = firsObject - 100;
            } else id = 1;

            if (id == 1) {
                showMessage(getResources().getString(R.string.start_list));
            } else {
                repoAdapter.getFavoriteRepo();
                presenter.getListRepo(firstNeedToFindRepoEdit.getText().toString(), secondNeedToFindRepoEdit.getText().toString(), id);
            }
        }

    }


    @OnClick(R.id.next100Repo)
    void onClickButtonNext() {
        // github api выдает по 100 объектов, поэтому для получения
        // следующих 100 объектов берем последной элемент из списка
        // и добавляем единицу
        if (firstNeedToFindRepoEdit.getText().toString().equals("") && secondNeedToFindRepoEdit.getText().toString().equals("")) {
            showMessage(getResources().getString(R.string.error_message_edit_null));
        } else {
            repoAdapter.getFavoriteRepo();
            int lastObject = repoAdapter.getLastObject();
            int id = lastObject + 1;
            presenter.getListRepo(firstNeedToFindRepoEdit.getText().toString(), secondNeedToFindRepoEdit.getText().toString(), id);
        }

    }

    @OnClick(R.id.buttonFavorite)
    void onClickButtonFavorite() {
        startActivityForResult(new Intent(getApplicationContext(), FavoriteListRepoActivity.class), COME_FROM_FAVORITE_ACTIVITY_CODE);
    }

    @Override
    public void repoClick(String repoUrl) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(repoUrl)));
    }

    @Override
    public void clickToFavorite(int id, String name, String description, String url) {
        presenter.addFavoriteRepoToDataBase(id, name, description, url, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == COME_FROM_FAVORITE_ACTIVITY_CODE) {
            repoAdapter.clearList();
            repoAdapter.getFavoriteRepo();
            presenter.getListRepo(firstNeedToFindRepoEdit.getText().toString(), secondNeedToFindRepoEdit.getText().toString(), repoAdapter.getFistObject());
        }


    }
}