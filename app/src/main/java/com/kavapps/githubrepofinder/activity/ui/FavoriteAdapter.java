package com.kavapps.githubrepofinder.activity.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kavapps.githubrepofinder.R;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;
import com.kavapps.githubrepofinder.view.FavoriteRepoView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    FavoriteAdapter.FavoriteAdapterListener listener;
    List<FavoriteRepoModel> listFavoriteRepo;

    public interface FavoriteAdapterListener {
        void repoClick(String repoUrl);
        void clickToDelete(int id);
    }

    public FavoriteAdapter(Context context, FavoriteAdapter.FavoriteAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        listFavoriteRepo = new ArrayList<>();
    }

    public void setListRepo(List<FavoriteRepoModel> list) {
            this.listFavoriteRepo.clear();
            this.listFavoriteRepo.addAll(list);
            notifyDataSetChanged();

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item, parent, false);
        return new RepoAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final RepoAdapter.Holder repoHolder = (RepoAdapter.Holder) holder;
        FavoriteRepoModel repo = listFavoriteRepo.get(position);
        repoHolder.repoName.setText((position + 1) + ". " + repo.getName());
        repoHolder.repoDescription.setText(repo.getDescription());
        repoHolder.imageFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_delete_forever_24));
        repoHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.repoClick(repo.getUrl());
            }
        });
        repoHolder.imageFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.clickToDelete(repo.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFavoriteRepo.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageFavorite)
        ImageView imageFavorite;

        @BindView(R.id.repoName)
        TextView repoName;

        @BindView(R.id.repoDescription)
        TextView repoDescription;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
