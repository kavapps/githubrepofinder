package com.kavapps.githubrepofinder.activity.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kavapps.githubrepofinder.R;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private AdapterListener listener;
    private List<PublicRepositoriesResponse> listRepo;

    private List<FavoriteRepoModel> favoriteRepoModelList = new ArrayList<>();
    private static final String DATA_BASE_NAME = "favoriteRepo";

    public interface AdapterListener {
        void repoClick(String repoUrl);

        void clickToFavorite(int id, String name, String description, String url);
    }

    public RepoAdapter(Context context, AdapterListener listener) {
        this.context = context;
        this.listener = listener;
        listRepo = new ArrayList<>();
    }

    public void setListRepo(PublicRepositoriesResponse repo) {
        equalsLists(listRepo,favoriteRepoModelList);
        boolean isFindInList = false;
        for (int i = 0; i < listRepo.size(); i++) {
            if (listRepo.get(i).getId() == repo.getId()) {
                isFindInList = true;
                break;
            }
        }
        if (!isFindInList) {
            this.listRepo.add(repo);
            notifyDataSetChanged();
        }

    }

    public void clearList() {
        this.listRepo.clear();
    }

    public void getFavoriteRepo(){
        getListFavoriteRepo();
    }

    public int getListSize() {
        if (listRepo.size() != 0) {
            return listRepo.size();
        } else return 1;
    }

    public int getFistObject() {
        if (listRepo.size() != 0) {
            return listRepo.get(0).id;
        } else return 1;
    }

    public int getLastObject() {
        if (listRepo.size() != 0) {
            return listRepo.get(listRepo.size() - 1).id;
        } else return 1;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_item, parent, false);
        return new RepoAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final RepoAdapter.Holder repoHolder = (RepoAdapter.Holder) holder;
        PublicRepositoriesResponse repo = listRepo.get(position);
        repoHolder.repoName.setText((position + 1) + ". " + repo.getName());
        repoHolder.repoDescription.setText(repo.getDescription());
        repoHolder.imageFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_star_border_24));
        repoHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.repoClick(repo.getHtmlUrl());
            }
        });
        repoHolder.imageFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!repo.isFavorite){
                    listener.clickToFavorite(repo.getId(), repo.getName(), repo.getDescription(), repo.getHtmlUrl());
                    repoHolder.imageFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_star_24));
                }
            }
        });

        for (int i=0;i<favoriteRepoModelList.size();i++){
            if(favoriteRepoModelList.get(i).getId()==repo.getId()){
                repo.setFavorite(true);
                repoHolder.imageFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_star_24));
            }else{
                repo.setFavorite(false);
                repoHolder.imageFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_baseline_star_border_24));
            }
        }

    }

    @Override
    public int getItemCount() {
        return listRepo.size();
    }

    public void getListFavoriteRepo() {
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child(DATA_BASE_NAME);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    favoriteRepoModelList.clear();
                    for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                        FavoriteRepoModel favoriteRepoModel = dataSnapshot.getValue(FavoriteRepoModel.class);
                        favoriteRepoModelList.add(favoriteRepoModel);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e("kav","ERROR: "+error.getMessage());
            }
        });
    }

    public void equalsLists(List<PublicRepositoriesResponse> repoList, List<FavoriteRepoModel> favoriteRepoLIst) {
        boolean found = false;
        for (PublicRepositoriesResponse object1 : repoList) {
            for (FavoriteRepoModel object2 : favoriteRepoLIst) {
                if (object1.getId() == object2.getId()) {
                    found = true;
                    //also do something
                    object1.setFavorite(true);
                    break;
                }else {
                    object1.setFavorite(false);
                }
            }
            if (!found) {
                //do something
            }
            found = false;

        }
    }

    public static class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageFavorite)
        ImageView imageFavorite;

        @BindView(R.id.repoName)
        TextView repoName;

        @BindView(R.id.repoDescription)
        TextView repoDescription;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
