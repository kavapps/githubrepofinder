package com.kavapps.githubrepofinder.activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.arellomobile.mvp.MvpActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.kavapps.githubrepofinder.App;
import com.kavapps.githubrepofinder.R;
import com.kavapps.githubrepofinder.activity.ui.FavoriteAdapter;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;
import com.kavapps.githubrepofinder.presenter.FavoriteListRepoPresenter;
import com.kavapps.githubrepofinder.view.FavoriteRepoView;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;

import java.util.List;

import javax.inject.Inject;

public class FavoriteListRepoActivity extends MvpActivity implements FavoriteAdapter.FavoriteAdapterListener, FavoriteRepoView, InternetConnectivityListener {

    @Inject
    @InjectPresenter
    FavoriteListRepoPresenter presenter;

    @ProvidePresenter
    FavoriteListRepoPresenter providePresenter() {
        return presenter;
    }

    @BindView(R.id.favoriteListRecyclerView)
    RecyclerView favoriteListRecyclerView;

    private FavoriteAdapter repoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_list_repo_activity);
        ButterKnife.bind(this);
        repoAdapter = new FavoriteAdapter(this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        favoriteListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        favoriteListRecyclerView.setLayoutManager(mLayoutManager);
        favoriteListRecyclerView.setAdapter(repoAdapter);

        InternetAvailabilityChecker mInternetAvailabilityChecker;
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);
    }

    @Override
    public void repoClick(String repoUrl) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(repoUrl)));
    }

    @Override
    public void clickToDelete(int id) {
        presenter.deleteRepoFromList(id);
    }

    @Override
    public void showListFavoriteRepo(List<FavoriteRepoModel> list) {
        repoAdapter.setListRepo(list);
    }

    @Override
    public void showMessage(String string) {
        Toast.makeText(getApplicationContext(),string,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            presenter.getListFavoriteRepo();
        }
        else {
            showMessage(getString(R.string.internet_connection_check));
        }

    }
}