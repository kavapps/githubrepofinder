package com.kavapps.githubrepofinder.api;

import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GitHubApi {

    @GET("/repositories")
    Observable<List<PublicRepositoriesResponse>> getPublicRepositories(
            @Query("since") int id
    );
}
