package com.kavapps.githubrepofinder;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.kavapps.githubrepofinder.di.component.AppComponent;
import com.kavapps.githubrepofinder.di.component.DaggerAppComponent;
import com.kavapps.githubrepofinder.di.module.GitHubApiModule;
import com.kavapps.githubrepofinder.di.module.InteractorsModule;
import com.kavapps.githubrepofinder.di.sub.ActivityComponent;

import androidx.appcompat.app.AppCompatDelegate;

public class App extends Application {


    private static AppComponent component;
    private static ActivityComponent activityComponent;
    private static final String LOG_TEG = "App";

    public static AppComponent getComponent() {
        return component;
    }

    public static ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        component = createComponent();
        activityComponent = createActivityComponent();
        registerActivityLifecycleCallbacks(new ScreenOnActivityLifecycleCallbacks());
    }


    private ActivityComponent createActivityComponent() {
        return component.plusActivityComponent(new InteractorsModule());
    }

    private AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .gitHubApiModule(new GitHubApiModule())
                .build();
    }

    private class ScreenOnActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        @Override
        public void onActivityStarted(Activity activity) {
            Log.e("kav", LOG_TEG + " onActivityStarted");
        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.e("kav", LOG_TEG + " onActivityResumed");
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Log.e("kav", LOG_TEG + " onActivityPaused");
        }

        @Override
        public void onActivityStopped(Activity activity) {
            Log.e("kav", LOG_TEG + " onActivityStopped");
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            Log.e("kav", LOG_TEG + " onActivitySaveInstanceState");
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            Log.e("kav", LOG_TEG + " onActivityDestroyed");
        }
    }
}
