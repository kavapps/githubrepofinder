package com.kavapps.githubrepofinder.di.module;


import com.kavapps.githubrepofinder.api.GitHubApi;
import com.kavapps.githubrepofinder.model.PublicRepoInteractor;
import com.kavapps.githubrepofinder.model.impl.PublicRepoInteractorImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class InteractorsModule {
    @Provides
    PublicRepoInteractor providePublicRepoInteractor(GitHubApi api) {
        return new PublicRepoInteractorImpl(api);
    }

}
