package com.kavapps.githubrepofinder.di.sub;


import com.kavapps.githubrepofinder.activity.FavoriteListRepoActivity;
import com.kavapps.githubrepofinder.activity.SearchRepoActivity;
import com.kavapps.githubrepofinder.di.module.InteractorsModule;

import dagger.Subcomponent;

@Subcomponent(modules = {InteractorsModule.class})
public interface ActivityComponent {

    void inject(SearchRepoActivity searchRepoActivity);

    void inject(FavoriteListRepoActivity favoriteListRepoActivity);
}
