package com.kavapps.githubrepofinder.di.component;

import com.kavapps.githubrepofinder.di.module.GitHubApiModule;
import com.kavapps.githubrepofinder.di.module.InteractorsModule;
import com.kavapps.githubrepofinder.di.sub.ActivityComponent;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {GitHubApiModule.class})
@Singleton
public interface AppComponent {
    ActivityComponent plusActivityComponent(InteractorsModule interactorsModule);
}
