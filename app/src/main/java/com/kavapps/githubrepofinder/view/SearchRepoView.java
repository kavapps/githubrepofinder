package com.kavapps.githubrepofinder.view;

import com.arellomobile.mvp.MvpView;
import com.kavapps.githubrepofinder.pojo.PublicRepositoriesResponse;

public interface SearchRepoView extends MvpView {

   void showListPublicRepo(PublicRepositoriesResponse repo);
   void showMessage(String string);
}
