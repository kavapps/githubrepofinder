package com.kavapps.githubrepofinder.view;

import com.arellomobile.mvp.MvpView;
import com.kavapps.githubrepofinder.firebase.FavoriteRepoModel;

import java.util.List;

public interface FavoriteRepoView extends MvpView {

    void showListFavoriteRepo(List<FavoriteRepoModel> list);
    void showMessage(String string);
}
